#include <iostream>
#include "geometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"


using namespace std;

int main(){
	Geometrica *formaGeometrica1 = new Geometrica();
	Geometrica formaGeometrica2(30, 20);
	Geometrica formaGeometrica3 = Geometrica(40, 50);

	Triangulo *triangulo1 = new Triangulo();
	Quadrado *quadrado1 = new Quadrado(50);
	Circulo *circulo1 = new Circulo(5);

	cout << "Area FormaGeometrica1 = " << formaGeometrica1 -> area() << endl;
	cout << "Area FormaGeometrica2 = " << formaGeometrica2.area() << endl;
	cout << "Area FormaGeometrica3 = " << formaGeometrica3.area() << endl;
	cout << "Area Triangulo1 = " << triangulo1 -> area() << endl;
	cout << "Area quadrado1 = " << quadrado1-> area() << endl;
	cout << "Area circulo1 = " << circulo1-> area() << endl;


	Geometrica *listaDeFormas[10] = {formaGeometrica1, triangulo1, quadrado1, circulo1};

	for(int i = 0; i < 4;i++){
		cout << "Area " << i << ":" << listaDeFormas[i]->area() << endl;
	}
}