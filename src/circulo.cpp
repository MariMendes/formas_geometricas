#include <iostream>
#include "circulo.hpp"

#define PI 3.1415 

using namespace std;

Circulo::Circulo(){
	setBase(10);
	setAltura(10);
}
Circulo::Circulo(float base){
	setBase(base);
	setAltura(base);
}
float Circulo::area(){
	return PI * getBase() * getBase();
}