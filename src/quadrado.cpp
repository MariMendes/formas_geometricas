#include <iostream>
#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado(){
	setBase(10);
	setAltura(10);
}
Quadrado::Quadrado(float base){
	setBase(base);
	setAltura(base);
}
float Quadrado::area(){
	return getBase() * getBase();
}