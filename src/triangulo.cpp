#include <iostream>
#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(){
	setBase(5);
	setAltura(15);
}
Triangulo::Triangulo(float base, float altura){
	setBase(base);
	setAltura(altura);
}
float Triangulo::area(){
	return ((getBase() * getAltura())/2.0f);
}