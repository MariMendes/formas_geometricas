#ifndef TRIANGULO_H
#define TRIANGULO_H
#endif

#include "geometrica.hpp"

class Triangulo: public Geometrica{
	public:
		Triangulo();
		Triangulo(float base, float altura);

		float area();
};
