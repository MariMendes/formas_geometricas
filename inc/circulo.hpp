#ifndef CIRCULO_H
#define CIRCULO_H
#include "geometrica.hpp"

class Circulo: public Geometrica{;
	public:
		Circulo();
		Circulo(float base);
		float area();

}; 
#endif