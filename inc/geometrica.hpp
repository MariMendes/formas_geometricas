#ifndef GEOMETRICA_H
#define GEOMETRICA_H

class Geometrica{
	private:
		float base, altura;
	public:

		void setBase(float base);
		void setAltura(float altura);

		Geometrica();
		Geometrica(float base, float altura);
		Geometrica(int base, int altura);

		float getBase();
		float getAltura();


		virtual float area()  = 0;
		/*essa diretiva e para dizer que se alguma classe
		 filha sobrescrever a classe filha sobrescrever a classe pai (essa), entao a sobrescrita vai ser usada.
		 o cpp do geometrica nao pode ter a funcao area(), por que ela foi anulada.  "area() = 0"*/
};
#endif